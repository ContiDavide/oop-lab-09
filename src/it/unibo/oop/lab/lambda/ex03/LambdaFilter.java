package it.unibo.oop.lab.lambda.ex03;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

/**
 * Modify this small program adding new filters.
 *
 * 1) Convert to lowercase
 *
 * 2) Count the number of chars
 *
 * 3) Count the number of lines
 *
 * 4) List all the words in alphabetical order
 *
 * 5) Write the count for each word, e.g. "word word pippo" should output "pippo
 * -> 1 word -> 2
 *
 */
public final class LambdaFilter extends JFrame {

    private static final long serialVersionUID = 1760990730218643730L;
    /*
     * This is a "regular expression". It is a very powerful tool for inspecting and
     * manipulating strings. Unfortunately, we have no room in this course to
     * introduce them - but you can read something yourself (start from
     * https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html), and
     * test your abilities with https://regex101.com/
     */
    private static final String ANY_NON_WORD = "(\\s|\\p{Punct})+";

    private enum Command {
        /**
         * Commands
         */
        IDENTITY("No modifications", Function.identity()), TO_LOWER("Lowercase", String::toLowerCase), COUNT(
                "Count chars", s -> Integer.toString(s.length())), LINES("Count lines",
                        s -> Long.toString(s.chars().filter(e -> e == '\n').count() + 1)), WORDS(
                                "Sort words in alphabetical order",
                                s -> Arrays.stream(s.split(ANY_NON_WORD)).sorted()
                                        .collect(Collectors.joining("\n"))), WORDCOUNT(
                                                "Count words",
                                                s -> Arrays.stream(s.split(ANY_NON_WORD))
                                                        .collect(Collectors.groupingBy(Function.identity(),
                                                                Collectors.counting()))
                                                        .entrySet().stream()
                                                        .map(e -> e.getKey() + " -> " + e.getValue())
                                                        .collect(Collectors.joining("\n")));
        /*Come ho fatto io --> molto male
         * IDENTITY("No modifications", Function.identity()),
        LOWERCASE("Lowercase", (x->x.toLowerCase())),
        COUNT_NUMBER_OF_CHARS("Count the number of the chars",x->{
            final int lengthOfString = x.length();
            return Integer.toString(lengthOfString);
        }),
        COUNT_NUMBER_OF_LINES("Count the number of the lines",x->{
            final String vett[] = x.split("\n");
            return Integer.toString(vett.length);
        }),
        LIST_WORDS_ALPHABETICAL_ORDER("List all the words in alphabetical order",x->{
                final List<String> vett = new ArrayList<>(Arrays.asList(x.split("\n")));
                String tmp=vett.get(0);
                for(int i=1;i<vett.size();i++) {
                    tmp = tmp + " " + vett.get(i);  
                }
                final List<String> vett1 = new ArrayList<>(Arrays.asList(tmp.split(" ")));
                vett1.sort((z,y)->z.compareTo(y));
                String out="";
                for(final String elem: vett1) {
                    out = out + "\n" + elem;  
                }
                return out.trim();
            }),
        COUNT_WORDS("Count each word",x->{
            final List<String> vett = new ArrayList<>(Arrays.asList(x.split("\n")));
            String tmp=vett.get(0);
            for(int i=1;i<vett.size();i++) {
                tmp = tmp + " " + vett.get(i);  
            }
            final List<String> vett1 = new ArrayList<>(Arrays.asList(tmp.split(" ")));
            final Map<String,Integer> ris = new HashMap<>();
            vett1.forEach(elem->{
                int v;
                if(ris.containsKey(elem)) {
                    v=ris.get(elem);
                    ris.put(elem, v+1);
                } else {
                    ris.put(elem,1);
                }
            });
            String out = "";
            for(Map.Entry<String, Integer> entry: ris.entrySet()) {
                out = out + "\n" + entry.getKey() + " -> " + entry.getValue();  
            }
            return out.trim();
        });
         * */
        

        private final String commandName;
        private final Function<String, String> fun;

        Command(final String name, final Function<String, String> process) {
            commandName = name;
            fun = process;
        }

        @Override
        public String toString() {
            return commandName;
        }

        public String translate(final String s) {
            return fun.apply(s);
        }

    }

    private LambdaFilter() {
        super("Lambda filter GUI");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final JPanel panel1 = new JPanel();
        final LayoutManager layout = new BorderLayout();
        panel1.setLayout(layout);
        final JComboBox<Command> combo = new JComboBox<>(Command.values());
        panel1.add(combo, BorderLayout.NORTH);
        final JPanel centralPanel = new JPanel(new GridLayout(1, 2));
        final JTextArea left = new JTextArea();
        left.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        final JTextArea right = new JTextArea();
        right.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        right.setEditable(false);
        centralPanel.add(left);
        centralPanel.add(right);
        panel1.add(centralPanel, BorderLayout.CENTER);
        final JButton apply = new JButton("Apply");
        apply.addActionListener((ev) -> right.setText(((Command) combo.getSelectedItem()).translate(left.getText())));
        panel1.add(apply, BorderLayout.SOUTH);
        setContentPane(panel1);
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        setSize(sw / 4, sh / 4);
        setLocationByPlatform(true);
    }

    /**
     * @param a
     *            unused
     */
    public static void main(final String... a) {
        EventQueue.invokeLater(() -> {
            final LambdaFilter gui = new LambdaFilter();
            gui.setVisible(true); 
        });
    }

}